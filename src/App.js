import TodoApp from './tp4/TodoApp'
import App3 from './tp3/App3';
import App2 from './tp2/App2';
import App1 from './tp1/App1';
import {BrowserRouter as Router,NavLink, Redirect} from 'react-router-dom';
import  Route from 'react-router-dom/Route';
import './navbar.css';

function App() {
  return (
   <>
   <Router>
   <div className="navbar">
        <ul className="links">
        <li><NavLink to="/tp1" className="link" activeStyle={{color:'white',backgroundColor:'rgb(230,230,230);'}}> tp1 </NavLink></li>
        <li><NavLink to="/tp2" className="link" activeStyle={{color:'white'}}> tp2 </NavLink></li>
        <li><NavLink to="/tp3" className="link" activeStyle={{color:'white'}}> tp3 </NavLink></li>
        <li><NavLink to="/tp4" className="link" activeStyle={{color:'white'}}> tp4 </NavLink></li>
        </ul>
        <div className="name"><h3>FAROUQ ilyass</h3></div>
        </div>
        <Route path='/tp1' render={()=><App1/>}/>
        <Route path='/tp2' render={()=><App2/>}/>
        <Route path='/tp3' render={()=><App3/>}/>
        <Route path='/tp4' render={()=><TodoApp/>}/>
        <Route path='/' render={()=><Redirect to="/tp1"/>}/>
    </Router>
   </>
  );
}

export default App;
