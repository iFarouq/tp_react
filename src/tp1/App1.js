import React,{useState,useEffect} from 'react';
import './App.css';


function App1() {
  const getIMC= v=>{  if(v<18.5) return <div style={{color: "red",fontWeight:"bold"}}>poids insuffisance</div>
                    if(v>18.5 && v<24.9) return<div style={{color: "green",fontWeight:"bold"}}> poids normal</div>
                    if(v>25 && v <29.9) return <div style={{color: "yellow",fontWeight:"bold"}}>surpoids</div>
                      if(v>30)return <div style={{color: "red",fontWeight:"bold"}}>obesite</div>
}
  const [poids,setPoids]=useState(0)
  const [taille,setTaille]=useState(0)
  const [resultat,setResultat]=useState({})
  const changePoids=e=>{setPoids(e.target.value)}
  const changeTaille=e=>{setTaille(e.target.value)}
  const submit=e=>{e.preventDefault();
                   let poids_=poids,taille_=taille;
                   if(!poids_ || !taille_){alert('error');return;}
                   let res=(poids_/(taille_**2)).toFixed(2)
                   setResultat({resultat:<h2>Le resultat est : {res} </h2> , IMC:getIMC(res)}) 
                  }
  return (
    <div className='app'>
      <div>
       <h1 className='title'> Calcul IMC </h1>
       <form onSubmit={submit}>
                            <div><input type='text' placeholder='Taille (m)' onChange={changeTaille} className='input'/></div>
                            <div><input type='text' placeholder='Poids (kg)' onChange={changePoids}  className='input'/></div>
                            <div><input type='reset' value='reinisialiser' onClick={_=>{setResultat({});setPoids(0);setTaille(0)}}/> <input type='submit' value='calculer'/></div>
        </form>
              
                            <div className='content'>
                            {resultat && resultat.resultat}
                            {resultat && resultat.IMC}
                            </div>
                   </div>         
               
    </div>
  );
 

  
}

export default App1;
