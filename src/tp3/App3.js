import React,{useState,useEffect} from 'react';
import './card.css';
import Card from './components/Card';
import axios from 'axios';

function App3() {

  let [repos,setRepos]=useState([])
  let [isLoading,setIsLoading]=useState(false)
  let [currentPage,setCurrentPage]=useState(1)
  let [totalPages,setTotalPages]=useState(0)
  useEffect(()=>{
  const getData=async()=>{
    setIsLoading(true)
    let url='https://api.github.com/search/repositories?q=created:>2020-11-05&sort=stars&order=DESC&page='+currentPage+'&per_page=7';
    let {items,total_count}=await axios.get(url).then(res=>res.data).catch(err=>err)
    //console.log(items)
    setTotalPages(total_count)
    setRepos([...(items|| '')]);
    setIsLoading(false)
    console.log(items)
  }
  getData()

  },[currentPage])
  const Paginate = page =>{ setCurrentPage(page)}
  const handle=(e)=>{/*let target=e.target;
  if(target.scrollHeight-target.scrollTop<1000) setCurrentPage(currentPage+1)*/
  // with infinite scroll
  }
  let content=isLoading?<div className="loading"><p>Loading...</p></div>:repos.map(repo=><Card avatar={repo.owner.avatar_url} name={repo.name} stars={repo.stargazers_count} open_issues={repo.open_issues_count} description={repo.description}/>)
  return ( <>
   <div className="pages">  {[...Array(20).keys()].map(k=><a onClick={()=>Paginate(k+1)} className={currentPage === k+1 && "active"}>{k+1}</a>)}</div>
   <div className="body">
  <div  className="container" onScroll={handle}>
  {/*repos.map(repo=><Card avatar={repo.owner.avatar_url} name={repo.name} stars={repo.stargazers_count} open_issues={repo.open_issues_count} description={repo.description}/>)*/}
  {content}
    </div>
    </div>
  
  </>

  )
}

export default App3;
