import React from 'react'

export default function Avatar({url}) {
    return (
        <div className="card_image">
    <img src={url} className="card_avatar"/>
        </div>
    )
}
