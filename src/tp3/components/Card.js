import React from 'react';
import '../card.css';
import Avatar from "./Avatar";
import Cardbody from "./Cardbody";
function Card(props) {
 
  return (
    <div className='card'>
       
       <Avatar url={props.avatar}/>
      <Cardbody {...props}/>
       </div>    
  );
}

export default Card;
