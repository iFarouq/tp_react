import React from 'react'

export default function Cardbody({name,description,stars,open_issues}) {
    return (
        <div className="card_body">
        <h2 className="cardtitle">{name} </h2>
         <p class="content"> {description} </p>
         <button type="button" className="btn btn-warning">Stars: {stars}</button>
         <button type="button" className="btn btn-info">Open issues : {open_issues}</button>
         <span className='submition'> </span>
    
      </div>
    )
}
