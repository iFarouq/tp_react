import Todo from './components/Todo'
import TodoForm from './components/TodoForm'
import './style.css'
import {useState,useEffect} from 'react'
import {useSelector} from 'react-redux'
import EditFormModal from './components/EditFormModal'
import Options from './components/Options'



function App4() {
   const {com:completedTodos,uncom:uncompletedTodos}=useSelector(state=>state.todos)
                .reduce((prev,todo)=>(prev[todo.done?'com':'uncom'].push(todo),prev),{com:[],uncom:[]})
   const [open,setOpen]=useState(false)
   const [currentid,setCurrentid]=useState(null)
   const closeModal=_=>setOpen(false)
   useEffect(() => {
      //
      localStorage.setItem("saved_todos",JSON.stringify([...uncompletedTodos,...completedTodos]))
   }, [uncompletedTodos,completedTodos])
   const renderTodos=(arr,title)=>{
    return (  
      <>
      <h3> {title}</h3>
      {arr.map(({content,done,id})=>(<Todo id={id} Key={id} setOpen={setOpen} content={content} done={done}
                               setCurrentid={setCurrentid} />))}
       </>)}
  return (
    <>
       {open &&  <EditFormModal closeModal={closeModal} id={currentid}/>}  
        <div className="container">
        <TodoForm/>
        <Options/>
          {uncompletedTodos.length && renderTodos(uncompletedTodos,'uncompleted todos.') || ''}
          {completedTodos.length &&  renderTodos(completedTodos,'completed todos.') || ''}                                                 
          {!uncompletedTodos.length && !completedTodos.length && <div> <h3>your todo list is empty!</h3></div>}
        </div>
    </>
  );
}

export default App4;
/* todos.slice().sort((a,b)=>a.done-b.done)
{(todos.length && (!todos[0].done && <h3> not Completed.</h3> || <h3>all completed  :)</h3> )) || ''}
        {todos.length && todos
                              .map(({content,done,id},index)=>(<>
                                                        {(index && done!==todos[index-1].done)?<h3>Completed.</h3>:''}
                                                          <Todo 
                                                          id={id}
                                                         setOpen={setOpen}
                                                          content={content} 
                                                          done={done} /></>)) || <div> <h4>your todo list is empty!</h4> </div>}
 */