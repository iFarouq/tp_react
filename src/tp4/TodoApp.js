import React from 'react';
import App4 from './App4';
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import todoReducer from './reducers/todoReducer'
const store=createStore(todoReducer)
 const TodoApp=_=> (<Provider store={store}><App4/></Provider>)
 export default TodoApp;



