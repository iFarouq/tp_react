import {DELETE_ALL_TYPE,ADD_TYPE,EDIT_TYPE,MARK_TYPE,MARK_ALL_TYPE,DELETE_TYPE} from "../constants";
export const addTodo=todo=>({type:ADD_TYPE,payload:{content:todo}})
export const deleteTodo=todoId=>({type:DELETE_TYPE,payload:{id:todoId}})
export const editTodo=(newval,todoId)=>({type:EDIT_TYPE,payload:{content:newval,id:todoId}})
export const markTodo=todoId=>({type:MARK_TYPE,payload:{id:todoId}})
export const markAllTodos=_=>({type:MARK_ALL_TYPE})
export const deleteAllTodos=_=>({type:DELETE_ALL_TYPE})