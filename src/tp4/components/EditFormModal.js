import React ,{useState} from 'react'
import {useDispatch,useSelector} from 'react-redux'
import {editTodo} from '../actions'
import './styles/modal.css'

export default function EditFormModal({closeModal,id}) {
     const closeModalIfClickedOutside=e=>e.currentTarget==e.target && closeModal()
     const initial_content=useSelector(state=>state.todos.find(todo=>todo.id===id)?.content)
     const [content,setContent]=useState(initial_content);
     const handleChange= e =>{setContent(e.target.value);}
     const dispatch = useDispatch();
     const handleEditTodo=_=>{
         if(!content || content==initial_content){return}
         dispatch(editTodo(content,id));
         closeModal();setContent('')}
    return (
        <div className={'modal open'} onClick={closeModalIfClickedOutside}>
            <div className="modal_body">
                 <div className="close" onClick={_=>closeModal()}>x</div>
                 <div className="title"> edit todo</div>
                 <input type='text' value={content} onChange={handleChange} placeholder=""/>
                 <button onClick={handleEditTodo}> save changes </button>
            </div>
            
        </div>
    )
}
