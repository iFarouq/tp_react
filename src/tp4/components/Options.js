import React from 'react'
import {useDispatch} from 'react-redux'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import {deleteAllTodos,markAllTodos} from '../actions'

export default function Options() {
 const dispatch = useDispatch()
    return (
        <div className="options">
                    <a onClick={()=>{dispatch(markAllTodos())}}>
                      mark all  <EditIcon/>
                    </a> 
                    <a onClick={()=>{dispatch(deleteAllTodos())}}>
                         delete all   <DeleteIcon/>
                            </a>
                    </div>
    )
}
