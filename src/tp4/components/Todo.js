import React from 'react'
import './styles/todo.css'
import {useDispatch} from 'react-redux'
import TodoOptions from '../components/TodoOptions'
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import{markTodo,deleteTodo} from '../actions'


export default function Todo({content,done,id,setOpen,setCurrentid}) {
    const dispatch = useDispatch()
    const handleDeleteTodo=id=>{dispatch(deleteTodo(id))}
    const handleMarkTodo=id=>{dispatch(markTodo(id))}
    const handleEditTodo=id=>{setOpen(true);setCurrentid(id)}
    return (
        <div className={"todo_container"+((done)?' checked':'')}>
            <div className="status">
                   {!done &&     <a href="#" onClick={e=>{e.preventDefault();handleMarkTodo(id)}}>
                              <ClearIcon style={{'fill':'rgb(112,112,112)'}}/>
                              </a> || <CheckIcon style={{'fill':'rgb(0,255,0)'}}/>}      
                 </div>
            <div className={"todo_content"+((done)?' checked':'')}> {content}</div>
            <TodoOptions handleDelete={_=>handleDeleteTodo(id)} handleEdit={_=>handleEditTodo(id)}/>
            
        </div>
    )
}
