import React,{useState} from 'react'
import {useDispatch} from 'react-redux'
import './styles/form.css'
import {addTodo} from '../actions'

export default function TodoForm() {
    const [content,setContent]=useState('')
    const dispatch = useDispatch()
    const handleAddTodo=e=>{
        e.preventDefault()
        if(!content) {alert('please write a content');return}
        dispatch(addTodo(content))
        setContent('')
                        }
    return (
       
       <div className="todo_form">
           <div className="todo_input">
            <input type="text"
            className="form_input"
            value={content}
            placeholder="write your todo"
            onChange={(e)=>setContent(e.target.value)}/>
            <label>todo</label>
            </div>
            <button onClick={e=>handleAddTodo(e)}>add todo</button>
        </div>
    )
}
