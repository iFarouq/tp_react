import React from 'react'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

export default function TodoOptions({handleEdit,handleDelete}) {
    return (
        <div className="actions">
        <a href="#" onClick={e=>{e.preventDefault();handleEdit()}}>
          <EditIcon/>
        </a>
        <a href="#"  onClick={e=>{e.preventDefault();handleDelete()}}>
            <DeleteIcon/>
          </a>
</div>
    )
}
