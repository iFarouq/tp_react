import {DELETE_ALL_TYPE,ADD_TYPE,EDIT_TYPE,MARK_TYPE,MARK_ALL_TYPE,DELETE_TYPE} from "../constants";
const initialState={todos:[]}
const getTodosFromLocalStorage=(callback)=>{
    callback(JSON.parse(localStorage.getItem("saved_todos")))
}
const initializeState=data=> { data && (initialState.todos=data)} 
console.log(getTodosFromLocalStorage(initializeState))

const todoReducer=(state=initialState,action)=>
{  switch (action.type) {
    case  DELETE_TYPE:
        return {...state,todos:state.todos.filter((todo)=>todo.id!==action.payload.id)}
        break;
    case ADD_TYPE:
     return {...state,todos:[...state.todos,{content:action.payload.content,done:false,id:Date.now()}]}
        break;
    case EDIT_TYPE:
        return {...state,todos:state.todos.map((todo)=>todo.id==action.payload.id?({...todo,content:action.payload.content}):todo)}
        break;
    case MARK_TYPE:
       return {...state,todos:state.todos.map((todo)=>todo.id==action.payload.id?({...todo,done:!todo.done}):todo)}
        break;
    case DELETE_ALL_TYPE:
        return {...state,todos:[]}
        break;                
    case MARK_ALL_TYPE:
        if(!state.todos.length) return {...state}
        return {...state,todos:state.todos.map(todo=>(!todo.done && (todo.done=true),todo))};
        break;
    default:
        return {...state}
        break;
}

}
export default todoReducer